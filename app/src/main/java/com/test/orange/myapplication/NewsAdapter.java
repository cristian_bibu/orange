package com.test.orange.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Cristi on 14-Feb-18.
 */

public class NewsAdapter extends ArrayAdapter<NewsItem>
{
    private Context mContext;
    private ArrayList<NewsItem> mNewsItems;
    public NewsAdapter(Context context, ArrayList<NewsItem> newsItems)
    {
        super(context, -1, newsItems);
        mContext = context;
        mNewsItems = newsItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ViewHolder viewHolder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.news_item, parent, false);

            viewHolder.image = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.description);
            viewHolder.pubDate = (TextView)convertView.findViewById(R.id.date);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.image.setImageBitmap(mNewsItems.get(position).image);
        viewHolder.title.setText(mNewsItems.get(position).title);
        viewHolder.description.setText(mNewsItems.get(position).description);
        viewHolder.pubDate.setText(mNewsItems.get(position).publicationDate);

        return convertView;
    }

    static class ViewHolder
    {
        TextView title;
        TextView description;
        TextView pubDate;
        ImageView image;
        int position;
    }
}
