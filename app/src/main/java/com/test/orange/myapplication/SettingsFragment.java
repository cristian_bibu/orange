package com.test.orange.myapplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class SettingsFragment extends Fragment
{
    private RadioGroup mRadioGroup;
    OnRadioSelectedListener mCallback;


    public SettingsFragment()
    {

    }

    @Override
    public  void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View root = inflater.inflate(R.layout.settings_fragment, container,false);
        Bundle bundle = getArguments();
        boolean openInWebView = bundle.getBoolean("OpenInWebView",false);
        mRadioGroup = root.findViewById(R.id.radioGroup);
        if (openInWebView)
        {
            mRadioGroup.check(R.id.webview);
        }
        else
        {
            mRadioGroup.check(R.id.browser);
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id)
            {
                switch(id)
                {
                    case R.id.browser:
                            SetBrowser();
                        break;
                    case R.id.webview:
                            SetWebview();
                        break;
                }
            }
        });
        return  root;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try {
            mCallback = (OnRadioSelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString());
        }
    }

    @Override
    public void onDetach()
    {
        mCallback = null;
        super.onDetach();
    }

    private void SetBrowser()
    {
        mCallback.OnSelectBrowser();
    }
    private void SetWebview()
    {
        mCallback.OnSelectWebview();
    }

    public interface OnRadioSelectedListener
    {
        public void OnSelectBrowser();
        public void OnSelectWebview();
    }

}
