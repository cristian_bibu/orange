package com.test.orange.myapplication;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Cristi on 13-Feb-18.
 */

public class Networkmanager extends Handler
{
    public static Networkmanager instance = null;
    private static Object lock = new Object();
    public static int SUCCESS = 200;
    ArrayList<WeakReference<INewsContainer>> mNewsObservers;

    private CacheManager mCacheManager;

    private Networkmanager()
    {
        mNewsObservers = new ArrayList<WeakReference<INewsContainer>>();
        mCacheManager = CacheManager.GetInstance();
    }

    public static Networkmanager GetInstance()
    {
        synchronized (lock)
        {
            if (instance == null)
            {
                instance = new Networkmanager();
            }
        }

        return  instance;
    }

    public void Register(INewsContainer container)
    {
        if (!mNewsObservers.contains(container))
        {
            mNewsObservers.add(new WeakReference<INewsContainer>(container));
        }
    }

    public void UnRegister(INewsContainer container)
    {
        if (mNewsObservers.contains(container)) {
            mNewsObservers.remove(container);
        }
    }

    public void UpdateNews()
    {
        GetNewsRequest gnr = new GetNewsRequest(this);
        gnr.UpdateNews();
    }

    @Override
    public void handleMessage(Message message)
    {
        if (message.obj instanceof GetNewsRequest)
        {
            GetNewsRequest gnr = (GetNewsRequest) message.obj;
            for (int i = 0; i < mNewsObservers.size(); i++)
            {
                INewsContainer obs = mNewsObservers.get(i).get();
                if (obs != null)
                {
                    obs.UpdateNews(gnr.GetNews());
                }
            }
        }
    }

    public void OnNewsUpdated(GetNewsRequest gnr)
    {
        mCacheManager.CacheNews(gnr.GetNews());
        Message message = obtainMessage(SUCCESS, gnr);
        message.sendToTarget();
    }

    public interface INewsContainer
    {
        public void UpdateNews(ArrayList<NewsItem> newsList);
    }
}
