package com.test.orange.myapplication;

import android.graphics.Bitmap;
import android.media.Image;

import java.io.Serializable;

/**
 * Created by Cristi on 13-Feb-18.
 */


public class NewsItem implements Serializable
{
    public String title;
    public String description;
    public String link;
    public String permaLink;
    public String publicationDate;
    public String imageUrl;
    transient public Bitmap image;

    public String imageName;

    public String downloadDate;
    public String urlPermalink;
    public String urlImage;
    public int imageWidth;
    public int imageHeight;
}
