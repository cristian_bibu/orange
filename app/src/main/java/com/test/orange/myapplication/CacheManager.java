package com.test.orange.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Cristi on 14-Feb-18.
 */

public class CacheManager
{
    private static CacheManager instance = null;
    private static Object lock = new Object();

    private static String NEWS_CACHE = "StoredBBCNews";
    private static String IMAGES_CACHE = "StoredBBCNewsImages";

    private Context mContext;

    private CacheManager()
    {

    }

    public static CacheManager GetInstance()
    {

        synchronized (lock)
        {
            if (instance == null)
            {
                instance = new CacheManager();
            }
        }

        return  instance;
    }

    public void UpdateContext(Context context)
    {
        mContext = context;
    }

    public ArrayList<NewsItem> GetCachedNews()
    {
        try
        {
            FileInputStream fis = mContext.openFileInput(NEWS_CACHE);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<NewsItem> newsList = (ArrayList<NewsItem>)ois.readObject();
            ois.close();
            fis.close();

            NewsItem newsItem;
            for (int i =0; i < newsList.size(); i++)
            {
                newsItem = newsList.get(i);
                newsItem.image = LoadImage(newsItem.imageUrl);
            }
            return newsList;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public void CacheNews(ArrayList<NewsItem> newsList)
    {
        try
        {
            FileOutputStream fos = mContext.openFileOutput(NEWS_CACHE, mContext.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(newsList);
            oos.close();
            fos.close();
            for (int i = 0 ; i < newsList.size(); i++)
            {
                SaveImage(newsList.get(i));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void SaveImage(NewsItem newsItem)
    {
        File dir = mContext.getDir(IMAGES_CACHE, mContext.MODE_PRIVATE);
        File file = new File(dir, Integer.toString(newsItem.imageUrl.hashCode()));
        if (!file.exists())
        {
            try {
                OutputStream os = new FileOutputStream(file);
                newsItem.image.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Bitmap LoadImage(String name)
    {
        Bitmap bitmap = null;
        File dir = mContext.getDir(IMAGES_CACHE, mContext.MODE_PRIVATE);
        File file = new File(dir, Integer.toString(name.hashCode()));
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                bitmap = BitmapFactory.decodeStream(fis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }
}
