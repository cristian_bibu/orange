package com.test.orange.myapplication;

/**
 * Created by Cristi on 15-Feb-18.
 */

public class DrawerItem
{
    public String name;
    public int stringId;


    public DrawerItem(String name, int stringId)
    {
        this.name = name;
        this.stringId = stringId;
    }
}
