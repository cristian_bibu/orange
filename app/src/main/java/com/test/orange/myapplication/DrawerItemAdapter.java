package com.test.orange.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Cristi on 15-Feb-18.
 */

public class DrawerItemAdapter extends ArrayAdapter<DrawerItem>
{
    private Context mContext;
    private ArrayList<DrawerItem> mDrawerItems;
    public DrawerItemAdapter(Context context, ArrayList<DrawerItem> drawerItems)
    {
        super(context, -1, drawerItems);
        mContext = context;
        mDrawerItems = drawerItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ViewHolder viewHolder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.drawer_item, parent, false);

            viewHolder.name = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.name.setText(mDrawerItems.get(position).name);

        return convertView;
    }

    static class ViewHolder
    {
        TextView name;
        int position;
    }
}
