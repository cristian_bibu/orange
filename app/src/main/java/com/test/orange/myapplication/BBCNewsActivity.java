package com.test.orange.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.RestrictionEntry;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

public class BBCNewsActivity extends AppCompatActivity implements Networkmanager.INewsContainer, SettingsFragment.OnRadioSelectedListener
{

    private final String OPEN_LINK_KEY = "DefaultOpenLink";
    private Networkmanager mNetworkmanager;
    private CacheManager mCacheManager;
    private ListView mListView;
    private ViewGroup mHeader;
    private ArrayList<NewsItem> mNewsList;
    private ListView mDrawerListView;
    private ArrayList<DrawerItem> mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawer;
    private ProgressBar mSpinner;
    private boolean mOpenInWebView;
    private SettingsFragment mSettingsFragment = null;
    private boolean mIsLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bbcnews);
        setContentView(R.layout.draww);

        mListView = (ListView)findViewById(R.id.newslist);
        mListView.setVisibility(View.GONE);
        mNetworkmanager = Networkmanager.GetInstance();
        mNetworkmanager.Register(this);
        mCacheManager = CacheManager.GetInstance();
        mCacheManager.UpdateContext(this);
        LayoutInflater layoutInflater = getLayoutInflater();
        mHeader = (ViewGroup) layoutInflater.inflate(R.layout.header,mListView,false);
        SetUpDrawerItems();
        mSpinner = (ProgressBar)findViewById(R.id.spinner);
        mSpinner.setVisibility(View.VISIBLE);
        LoadOpenLinkDefault();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mIsLoading = false;
        ArrayList<NewsItem> newsList = mCacheManager.GetCachedNews();
        if (newsList != null)
        {
            SetUpListView(newsList, true);
        }
        else
        {
            RefreshApp();
        }
    }

    @Override
    public void UpdateNews(ArrayList<NewsItem> newsList)
    {
        Log.d("",""+newsList.size());
        if (mSettingsFragment != null)
        {
            getSupportFragmentManager().beginTransaction().remove(mSettingsFragment).commit();
        }
        if (newsList != null)
        {
            mIsLoading = false;
            SetUpListView(newsList, false);
        }
    }

    private void SetUpListView(ArrayList<NewsItem> newsList, boolean isOffline)
    {
        NewsAdapter newsAdapter = new NewsAdapter(this, newsList);
        mListView.setAdapter(newsAdapter);
        if (isOffline)
        {
            mListView.addHeaderView(mHeader);
        }
        else
        {
            mListView.removeHeaderView(mHeader);
        }

        mListView.setVisibility(View.VISIBLE);
        mSpinner.setVisibility(View.GONE);
        mNewsList = newsList;
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                if (mOpenInWebView)
                {
                    Intent intent = new Intent(BBCNewsActivity.this, WebviewActivity.class);
                    intent.putExtra("URL", mNewsList.get(i).link);
                    startActivity(intent);
                }
                else
                {
                    Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(mNewsList.get(i).link));
                    if (IsIntentAvailable(BBCNewsActivity.this,intent))
                    {
                        startActivity(intent);
                    }
                }

            }
        });
    }

    private boolean IsIntentAvailable(Context ctx, Intent intent)
    {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list = mgr.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void SetUpDrawerItems()
    {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawer = (RelativeLayout) findViewById(R.id.drawerPanel);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerList = new ArrayList<DrawerItem>();
        DrawerItem di = new DrawerItem(getString(R.string.news),R.string.news);
        mDrawerList.add(di);
        di = new DrawerItem(getString(R.string.settings),R.string.settings);
        mDrawerList.add(di);
        di = new DrawerItem(getString(R.string.about),R.string.about);
        mDrawerList.add(di);
        mDrawerListView = (ListView)findViewById(R.id.navList);
        DrawerItemAdapter drawerAdapter = new DrawerItemAdapter(this, mDrawerList);
        mDrawerListView.setAdapter(drawerAdapter);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                int nameId = mDrawerList.get(position).stringId;
                switch (nameId)
                {
                    case R.string.news:
                        RefreshApp();
                        break;
                    case R.string.settings:
                        OpenSettings();
                        break;
                    case R.string.about:
                        OpenAbout();
                        break;
                }
                mDrawerLayout.closeDrawer(mDrawer);
            }
        });
    }

    private void RefreshApp()
    {
        mIsLoading = true;
        mListView.setVisibility(View.GONE);
        mSpinner.setVisibility(View.VISIBLE);
        ClearCachedNews();
        mNetworkmanager.UpdateNews();
    }

    private void ClearCachedNews()
    {
        NewsItem newsItem;
        for (int i = 0 ; i < mNewsList.size(); i++)
        {
            newsItem = mNewsList.get(i);
            newsItem.image = null;
            newsItem = null;
        }
        System.gc();
    }

    @Override
    public void OnSelectBrowser()
    {
        mOpenInWebView = false;
        getSupportFragmentManager().beginTransaction().remove(mSettingsFragment).commit();
        if (mIsLoading)
        {
            mListView.setVisibility(View.GONE);
            mSpinner.setVisibility(View.VISIBLE);
        }
        else
        {
            mListView.setVisibility(View.VISIBLE);
            mSpinner.setVisibility(View.GONE);
        }
        SaveOpenLinkDefault();
    }

    @Override
    public void OnSelectWebview()
    {
        mOpenInWebView = true;
        getSupportFragmentManager().beginTransaction().remove(mSettingsFragment).commit();
        if (mIsLoading)
        {
            mListView.setVisibility(View.GONE);
            mSpinner.setVisibility(View.VISIBLE);
        }
        else
        {
            mListView.setVisibility(View.VISIBLE);
            mSpinner.setVisibility(View.GONE);
        }
        SaveOpenLinkDefault();
    }

    private void SaveOpenLinkDefault()
    {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(OPEN_LINK_KEY,mOpenInWebView);
        editor.commit();
    }

    private void LoadOpenLinkDefault()
    {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        mOpenInWebView = sharedPref.getBoolean(OPEN_LINK_KEY,true);
    }

    private void OpenSettings()
    {
        mListView.setVisibility(View.GONE);
        mSpinner.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        mSettingsFragment = new SettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("OpenInWebView",mOpenInWebView);
        mSettingsFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.mainContent,mSettingsFragment);
        fragmentTransaction.commit();
    }

    private void OpenAbout()
    {
        Intent intent = new Intent(BBCNewsActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}