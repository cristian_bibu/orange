package com.test.orange.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Xml;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Cristi on 13-Feb-18.
 */

public class GetNewsRequest implements Runnable
{
    private static final String BBC_URL = "http://feeds.bbci.co.uk/news/rss.xml";
    private Networkmanager mNetworkManager;
    private Thread mThread;

    private ArrayList<NewsItem> mNewsItemsList;
    public GetNewsRequest(Networkmanager nm)
    {
        mNetworkManager = nm;
    }

    public void UpdateNews()
    {
        mThread = new Thread(this);
        mThread.start();
    }

    @Override
    public void run()
    {
        try
        {
            URL url = new URL(BBC_URL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setConnectTimeout(10000);
            InputStream is = connection.getInputStream();

            ProcesResponse(is);
            is.close();
            mNetworkManager.OnNewsUpdated(this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mNetworkManager.OnNewsUpdated(this);
        }
    }

    private void ProcesResponse(InputStream stream)
    {
        mNewsItemsList = new ArrayList<>();
        NewsItem newsItem = null;
        try
        {
            XmlPullParser xmlParser = Xml.newPullParser();
            xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlParser.setInput(stream, null);

            boolean isItem = false;
            while (xmlParser.next() != XmlPullParser.END_DOCUMENT)
            {
                int eventType = xmlParser.getEventType();
                String name = xmlParser.getName();
                if(name == null)
                {
                    continue;
                }

                if(eventType == XmlPullParser.END_TAG)
                {
                    if(name.equalsIgnoreCase("item"))
                    {
                        isItem = false;
                        mNewsItemsList.add(newsItem);
                        newsItem = null;
                    }
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG)
                {
                    if(name.equalsIgnoreCase("item"))
                    {
                        isItem = true;
                        newsItem = new NewsItem();
                        continue;
                    }
                }

                String result = "";
                if (xmlParser.next() == XmlPullParser.TEXT)
                {
                    result = xmlParser.getText();
                    xmlParser.nextTag();
                }

                if (isItem)
                {
                    if (name.equalsIgnoreCase("title"))
                    {
                        newsItem.title = result;
                    } else if (name.equalsIgnoreCase("description"))
                    {
                        newsItem.description = result;
                    } else if (name.equalsIgnoreCase("link"))
                    {
                        newsItem.link = result;
                    } else if (name.equalsIgnoreCase("guid"))
                    {
                        String tst = xmlParser.getAttributeValue(null, "isPermaLink");
                        boolean ispermaLink = Boolean.parseBoolean(xmlParser.getAttributeValue(null, "isPermaLink"));
                        if (ispermaLink) {
                            newsItem.permaLink = result;
                        } else {
                            newsItem.permaLink = result;
                        }
                    } else if (name.equalsIgnoreCase("pubDate"))
                    {
                        newsItem.publicationDate = result;
                    } else if (name.equalsIgnoreCase("media:thumbnail"))
                    {
                        newsItem.imageWidth = Integer.parseInt(xmlParser.getAttributeValue(null, "width"));
                        newsItem.imageHeight = Integer.parseInt(xmlParser.getAttributeValue(null, "height"));
                        newsItem.imageUrl = xmlParser.getAttributeValue(null, "url");
                        if (newsItem.imageUrl != null)
                        {
                            newsItem.image = CacheManager.GetInstance().LoadImage(newsItem.imageUrl);
                            if (newsItem.image == null)
                            {
                                newsItem.image = GetBitmapFromURL(newsItem.imageUrl,newsItem.imageWidth, newsItem.imageHeight);
                                CacheManager.GetInstance().SaveImage(newsItem);
                            }
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {

        }
    }

    public Bitmap GetBitmapFromURL(String path, int w, int h)
    {
        try
        {
            URL url = new URL(path);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setConnectTimeout(10000);
            InputStream is = connection.getInputStream();
            Bitmap b = DecodeBitmap(is, w,h);
            is.close();
            return b;
        }
        catch (Exception e)
        {

        }
        return null;
    }

    private Bitmap DecodeBitmap(InputStream is, int width, int height)
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.outWidth = width;
        options.outHeight = height;
        options.inSampleSize = CalculateSampleSize(options, 100, 100);
        return BitmapFactory.decodeStream(is,new Rect(0,0,100,100),options);
    }

    private static int CalculateSampleSize(BitmapFactory.Options options, int width, int height)
    {
        final int w = options.outWidth;
        final int h = options.outHeight;
        int inSampleSize = 1;

        if (w > width || h > height)
        {
            final int halfWidth = w / 2;
            final int halfHeight = h / 2;

            while ((halfWidth / inSampleSize) >= width &&  (halfHeight / inSampleSize) >= height)
            {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public ArrayList<NewsItem> GetNews()
    {
        return  mNewsItemsList;
    }
}
